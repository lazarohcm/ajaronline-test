-- List the name of all tenants that are renting more than one apartment.
SELECT tenant.TenantName
FROM Tenants tenant
INNER JOIN ApartmentsTenants at ON at.TenantID = tenant.TenantID
INNER JOIN Apartments ap ON ap.ApartmentID = at.ApartmentID
GROUP by tenant.TenantID
HAVING COUNT(ap.ApartmentID) > 1;

-- List the names of all buildings along with a count of how many open
-- requests that building has (`Requests`.`Status` = 'Open').
SELECT b.BuildingName, COUNT(r.RequestID) 'Open Requests'
FROM Requests r
INNER JOIN Apartments ap ON ap.ApartmentID = r.ApartmentID
INNER JOIN Buildings b on b.BuildingID = ap.BuildingID
WHERE r.status = 'Open'
GROUP BY b.BuildingID;


-- Close (`Requests`.`Status` = 'Closed') all open requests associated with building ID 42.
UPDATE Requests r
INNER JOIN Apartments ap ON ap.ApartmentID = r.ApartmentID
INNER JOIN Buildings b ON b.BuildingID = ap.BuildingID
SET r.Status = 'Closed'
WHERE r.Status = 'Open'
AND b.BuildingID = 42;


