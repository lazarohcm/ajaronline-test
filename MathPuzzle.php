<?php
/**
 * Sum numbers
 *  S = n*(n+1)/2
 */
$x = 13;
$y = 15;

echo sum($y) - sum($x);

/**
 * @param $value
 * @return int
 */
function sum($value): int
{
    return $value / 2 * ($value + 1);
}