# Ajar online test
## Concepts
1. Cors
It prevents http requests from non-authorized or recognized sources to a different origin.

2. CRSF
Force users with access privilege, in a certain website, to execute actions on the behalf of the attacker without the user knowing. 

3. XSS
Uses vulnerabilities to add malicious code in a website, attacking the users of that application.

4. JWT
A JSON token that is used for authentication in web application. This token can be encrypted 
using different algorithms, like RS256 or HS256. 
It contains a ``header`` with descriptive information, a ``body`` with the payload and a ``signature`` for decoding and validation.

5. PWAs & TWAs (Never heard of TWA)
PWAs are application run in a browser, making them a cross platform application. As long as the user have a browser, they can run the application.
They are able to run in offline mode and sync data with the server whenever the user has internet connection. 

6. SPAs
Applications that run in a single page, normally a ``index.html`` that rewrites it self, and multiple routes managed by a javascript framework. 
The application state is also managed via javascript. 

7. SSR & Pre-rendering
Server side rendering are application where its content is generated on the server side, making its content only available once all
processing is done and making it easier for indexing.

Pre-rendering is very useful for SEO purposes in single page applications, where the page is rendered on the user side 
and thus can't be properly indexed by search engines.
It make the content available and indexable for search engines, by pre-rendering it in different files.

## Logic
[FizzBuzz](https://bitbucket.org/lazarohcm/ajaronline-test/src/master/FizzBuzz.php)

[Math Puzzle](https://bitbucket.org/lazarohcm/ajaronline-test/src/master/MathPuzzle.php)

[Data Structures](https://bitbucket.org/lazarohcm/ajaronline-test/src/master/DataStrucute.php)

[Database](https://bitbucket.org/lazarohcm/ajaronline-test/src/master/database.sql)
 

