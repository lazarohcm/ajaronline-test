<?php
/**
 * JsonSchema to be mapped
 */
$scheme = '{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "type": "object",
  "properties": {
    "fruits": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "vegetables": {
      "type": "array",
      "items": {
        "$ref": "#/definitions/veggie"
      }
    }
  },
  "definitions": {
    "veggie": {
      "type": "object",
      "required": [
        "veggieName",
        "veggieLike"
      ],
      "properties": {
        "veggieName": {
          "type": "string",
          "description": "The name of the vegetable."
        },
        "veggieLike": {
          "type": "boolean",
          "description": "Do I like this vegetable?"
        }
      }
    }
  }
}';

$structure = json_decode($scheme, true);

/**
 * Recursively map the structure to its canonical structure
 */
$dataStructure = json_encode(mapProperties($structure, true));

echo $dataStructure;

/**
 * @param $structure
 * @param bool $toObject
 * @return array|stdClass
 */
function mapProperties(array $structure, bool $toObject = false)
{
    //Map as array
    if (!$toObject) {
        $dataStructure = [];

        foreach ($structure['properties'] as $key => $property) {

            $dataKey = [$key => mapType($property['type'], $property, $structure)];

            $dataStructure[] = $dataKey;
        }


        //Map as object
    } else {
        $dataStructure = new stdClass();

        foreach ($structure['properties'] as $key => $property) {
            $dataStructure->{$key} = mapType($property['type'], $property, $structure);
        }
    }
    return $dataStructure;

}

/**
 * @param $type
 * @param $property
 * @param $structure
 * @return array|mixed|null
 */
function mapType(string $type, array $property, array $structure)
{
    if ($type !== "array" && $type !== "object") {
        return $property['type'];
    }

    if ($type == "array" && !empty($property['items']['type'])) {
        return [mapType($property['items']['type'], $property['items'], $structure)];
    }

    if ($type == "array" && !empty($property['items']['$ref'])) {
        $path = explode("/", $property['items']['$ref']);
        return [mapObject($path, $structure)];
    }
}

/**
 * @param array $path
 * @param array $structure
 * @return array|null
 */
function mapObject(array $path, array $structure)
{
    $innerProperties = $structure[$path[1]][$path[2]];
    if (empty($innerProperties)) {
        return null;
    }

    return mapProperties($innerProperties, true);
}
